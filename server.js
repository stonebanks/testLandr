const express = require('express');
const config = require('./configure');

let app = config(express());

app.listen(app.get('port'), ()=>{
  console.log(`Server up: http://localhost:${app.get('port')}`);
});
