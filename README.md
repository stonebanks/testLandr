# testLandr

## Description

Basic REST API written using NodeJS. Those are the following endpoints to which the server responds:

| Http Methods | URI path | Arguments   | Description                               | Result                                   |
| ------------ | -------- | ----------- | ----------------------------------------- | ---------------------------------------- |
| GET          | /        | -           | Returns the caller IP Address             | ` {ip: <caller IP address>}`             |
| POST         | /        | ip, locale | Get the geolocation to all `IP` addresses | `[{ip: <IP>, location: <Country> },...]` |



### Example with curl 

``` bash
$ curl -d 'ip=1.1.1.1'\
       -d 'ip=42.42.42.42'\
       -d 'locale=fr'  http://localhost:3500 \
  # Should return [{"ip":"1.1.1.1","location":"Australie"},{"ip":"42.42.42.42","location":"Corée du Sud"}]
```


## Running the server

To run the server, since the docker image has been uploaded on Docker Hub, one can just type:

```bash
$ docker run -it --rm  --name testlandr -p 3500:3500 banksthemegalith/testlandr
```
and make the request with curl to http://localhost:3500

## Building the images locally

``` bash
$ git clone git@gitlab.com:stonebanks/testLandr.git
$ cd testlandr
$ docker build -t my-test-landrapp .
$ docker run -it --rm  --name testlandr -p 3500:3500 my-test-landrapp
```

### Discussion

The image is based upon a Debian image, it would have been less overkill to use a Node image. The reason behind that choice was motivated by the desire to free the git repo from having the GeoLited database commited.

## Live version

The request can be made against : http://thisisatest.us-east-2.elasticbeanstalk.com 