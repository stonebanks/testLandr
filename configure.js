const bodyParser = require('body-parser');
const routes = require('./routes');

module.exports = (app) => {
  app.set('port', process.env.PORT || 3500);
  app.set('GeoLite2DB', './resources/GeoLite2-Country/GeoLite2-Country.mmdb');
  app.use(bodyParser.urlencoded({extended: false}));
  app.use(bodyParser.json());
  app.use(routes(app));
  return app;
};
