FROM debian:buster

#install dependencies
RUN apt-get -yqq update
RUN apt-get -yqq install curl tar gnupg
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash
RUN apt-get install -yq nodejs

ADD . /opt/testLandR
WORKDIR /opt/testLandR

COPY package.json .
RUN npm install --silent
RUN mkdir -p resources/GeoLite2-Country
RUN curl -sS http://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.tar.gz -o - | tar xz --strip-component=1 -C resources/GeoLite2-Country

EXPOSE 3500
CMD node server.js
