const express = require('express');
const router = new express.Router();
const ipware = require('ipware');
const _ = require('lodash');
const maxmind = require('maxmind');

const maxmindOpenCbk = function(err, lookup, req, res) {
  if (err) {
    res.status(500).json({error: err.message});
  } else {
    try {
      let json = _.map(_.castArray(req.body.ip), (value) => {
        let lookedupvalue = lookup.get(value);
        if (maxmind.validate(value) && lookedupvalue) {
          let country = lookup.get(value).country;
          return {
            ip: value,
            location: country.names[req.body.locale] || country.names.en,
          };
        } else if (!lookedupvalue) {
          throw {message: `There was an issue in GeoLite DB with ${value}`};
        } else {
          throw {message: `${value} not valid IP address`};
        }
      }
      );
      res.json(json);
    } catch (err) {
      res.status(400).json({error: err.message});
    }
  }
};

const notfound = function(req, res) {
  res.status(404).json({error: 'not found'});
};

module.exports = (app) => {
  router.get('/', (req, res) => {
    // ipware returns a hybrid form with ipv6+v4
    const re = /(.*:)([\d{3}\.]+)$/;
    res.json({
      ip: ipware().get_ip(req).clientIp.replace(re, '$2'),
    });
  });

  router.post('/', (req, res) => {
    if (req.body.ip) {
      maxmind.open(
          app.get('GeoLite2DB'),
          _.partial(maxmindOpenCbk, _, _, req, res)
      );
    } else {
      res.status(500).json({error: 'request should have at least one ip addressin its body'});
    }
  });

  router.post('/*', notfound);
  router.get('/*', notfound);

  return router;
};
